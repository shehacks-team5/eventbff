You will need to add your SSH key to gitlab to clone the repo.
Go to https://gitlab.com/profile/keys to do so.

Working on Ubuntu 16.04, adapt as needed for your OS!

Install OS packages:

    apt-get install python3-dev python3-pip python-virtualenv
    apt-get install libpq-dev postgresql-contrib postgresql-server-dev-9.5
    apt-get install nginx git


To install:

    workon eventbff # or activate your virtualenv
    cd /opt
    git clone git@gitlab.com:shehacks-team5/eventbff.git
    pip install -r requirements.txt
    cp eventbff/eventbff.nginxconfig /etc/nginx/sites-available/eventbff
    cd /etc/nginx/sites-enabled
    ln -s ../sites-available/eventbff


Almost ready to run:

    workon eventbff
    cd /opt/eventbff/mysite/
    python manage.py makemigrations
    python manage.py migrate


To run:

    gunicorn mysite.wsgi:application --bind 127.0.0.1:8001
    # Ctrl+Z to stop, then 'bg' to background. 'fg' to foreground again.


Useful things to do occasionally:

    python manage.py creatersuperuser # interactive, for django admin
    python manage.py collectstatic # puts static assets in /opt/eventbff/static?
    python manage.py diffsettings # shows what settings are being used
