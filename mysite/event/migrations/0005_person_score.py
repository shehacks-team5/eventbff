# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-07-24 00:12
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0004_auto_20160724_0009'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='score',
            field=models.IntegerField(default=0),
        ),
    ]
