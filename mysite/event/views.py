from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic

from .models import Event, Item, Request, Offer, Person


class IndexView(generic.ListView):
    template_name = 'index.html'
    context_object_name = 'events'

    def get_queryset(self):
        """Return the last five published questions."""
        return Event.objects.order_by('-start_date')[:5]


class DetailView(generic.DetailView):
    model = Event
    template_name = 'detail.html'
    context_object_name = 'event'


def ask(request, slug):
    event = Event.objects.get(slug=slug)
    items = event.items.all
    return render(request, 'ask.html', {'items': items,
                                        'event': event,
    })


def give(request, slug):
    event = Event.objects.get(slug=slug)
    requests = Request.objects.filter(status='O').order_by('-start_time')
    return render(request, 'give.html', {'requests': requests,
                                         'event': event})


def asking(request, slug, id):
    current_user = request.user
    person = Person.objects.get(user=current_user)
    event = Event.objects.get(slug=slug)
    item = Item.objects.get(id=id)
    req = Request.objects.create(requester=person,
                                 item=item,
                                 details='')
    return render(request, 'asking.html', {'req': req,
                                         'event': event})


def giving(request, slug, id):
    current_user = request.user
    person = Person.objects.get(user=current_user)
    event = Event.objects.get(slug=slug)
    req = Request.objects.get(id=id)
    offer = Offer.objects.create(request=req,
                                 responder=person)
    return render(request, 'asking.html', {'req': req,
                                           'offer': offer,
                                         'event': event})
