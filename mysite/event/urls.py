from django.conf.urls import url

from . import views

urlpatterns = [
    # ex: /event/
    url(r'^$', views.IndexView.as_view(), name='index'),
    # ex: /event/grandeur-in-the-lawn/
    url(r'^(?P<slug>[-\w]+)/$', views.DetailView.as_view(), name='detail'),
    url(r'^(?P<slug>[-\w]+)/ask$', views.ask, name='ask'),
    url(r'^(?P<slug>[-\w]+)/give$', views.give, name='give'),
    url(r'^(?P<slug>[-\w]+)/ask/(?P<id>[0-9]+)$', views.asking, name='asking'),
    url(r'^(?P<slug>[-\w]+)/give/(?P<id>[0-9]+)$', views.giving, name='giving'),

]
