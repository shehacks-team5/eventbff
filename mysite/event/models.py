from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User


class Person(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    mobile = models.CharField(max_length=12)
    location = models.CharField(max_length=100, blank=True)
    last_login = models.DateTimeField(auto_now=True)
    score = models.IntegerField(default=0)
    # events_organising
    # events_attending

    def __str__(self):
        return self.user.username


class Item(models.Model):
    """A thing or action that can be requested"""
    EXPIRY = 15 * 60
    slug = models.SlugField(unique=True)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=600)
    icon = models.FileField()
    is_for_attendees = models.BooleanField(default=True)
    is_for_organisers = models.BooleanField(default=False)
    is_sos = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Event(models.Model):
    slug = models.SlugField(unique=True)
    name = models.CharField(max_length=100)
    start_date = models.DateField('Event start date')
    end_date = models.DateField('Event end date')
    location = models.CharField(max_length=100)
    description = models.CharField(max_length=600)
    rewards = models.CharField(max_length=1000, blank=True)
    items = models.ManyToManyField(Item)
    organisers = models.ManyToManyField(Person, related_name='events_organising')
    attendees = models.ManyToManyField(Person, related_name='events_attending', blank=True)
    logo = models.FileField(blank=True)
    is_approved = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Request(models.Model):
    """A specific request from a person"""
    requester =  models.ForeignKey(Person, on_delete=models.CASCADE)
    item = models.ForeignKey(Item)
    details = models.CharField(max_length=600, blank=True)
    start_time = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=20,
                              choices=(('O', 'open'),
                                       ('C', 'cancelled'),
                                       ('E', 'expired'),
                                       ('F', 'fulfilled')),
                              default='O',
    )


class Offer(models.Model):
    """A response to a request"""
    request = models.ForeignKey(Request, on_delete=models.CASCADE)
    responder = models.ForeignKey(Person, on_delete=models.CASCADE)
    status = models.CharField(max_length=20,
                              choices=(('O', 'open'),
                                       ('A', 'accepted'),
                                       ('D', 'declined'),
                                       ('C', 'cancelled'),
                                       ('E', 'expired')),
                              default='O',
    )

    
# Feedback -
# Person
# Thumbs up/down
# Timestamp
# Comment field
