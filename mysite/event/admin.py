from django.contrib import admin

from .models import Person, Item, Event, Request, Offer

admin.site.register(Person)
admin.site.register(Event)
admin.site.register(Item)
admin.site.register(Request)
admin.site.register(Offer)
